<!DOCTYPE html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DiskusiKan! - Home</title>

    
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/shop-homepage.css" rel="stylesheet">
    
</head>

<body>
    
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
		
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
			
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
					<li>
						<a href="home2.php"><img src="logosdf.png" style="width:170px; height:20px"></a>
					</li>
                    <li>
                        <a href="school.php">Sekolah</a>
                    </li>
                    <li>
                        <a href="informasi.php">Informasi & Peristiwa</a>
                    </li>
                    <li>
                        <a href="profil.php">Profil</a>
                    </li>
                </ul>
				<ul class="nav navbar-nav navbar-right">
						<li><a href="home.php"><span class="glyphicon glyphicon-log-in"></span> Keluar</a></li>
				</ul>
            </div>
        </div>
    </nav>
	
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead" align="center">Forum Diskusi</p>
                <div class="list-group">
                    <a href="forumpendidikan/ForumPendidikan.html" class="list-group-item">Pendidikan</a>
                    <a href="forumpolitik/ForumPolitik.html" class="list-group-item">Politik</a>
                    <a href="forumhiburan/ForumHiburan.html" class="list-group-item">Hiburan</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
								<li data-target="#carousel-example-generic" data-slide-to="3"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="SMKN1LGBTls.jpg" style="height:320px" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="SMKARJUNAls.jpg" style="height:320px"" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="SUDls.jpg" style="height:320px" alt="">
                                </div>
								<div class="item">
                                    <img class="slide-image" src="SMANSALGBTld.png" style="height:320px" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>
					
                </div>
	<div class="col-md-16">
		<div class="well" id="box" style="text-align: justify; font-size: 1em"> 
	    	<p>Ayo !!!</p>
			<p>Diskusikan segala hal yang ingin kalian diskusikan !!!</p>
			<p>Berbagi wawasan dan pengalaman lebih cepat dengan "DiskusiKan!"</p>
		</div> 
	</div>

            </div>

        </div>

    </div>

    <div class="container">

        <hr>

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; TheroMedian 2017</p>
                </div>
            </div>
        </footer>

    </div>

    <script src="js/jquery.js"></script>

    <script src="js/bootstrap.min.js"></script>

</body>

</html>
