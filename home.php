<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DiskusiKan! - Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/logincss.css" rel="stylesheet">

    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>


		<br>
        <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <a href="#"><img src="logosdf2.png" style="width:450px; height:70px"></a>
            </div>
        </div>
		<br>
        <!-- /.row -->

        <!-- Portfolio Item Row -->
        <div class="row">

            <div class="col-md-8">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
						<li data-target="#carousel-example-generic" data-slide-to="3"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img class="img-responsive" src="SMKN1LGBTls.jpg" style="height:320px" alt="">
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="SMKARJUNAls.jpg" style="height:320px" alt="">
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="SUDls.jpg" style="height:320px" alt="">
                        </div>
						<div class="item">
                            <img class="img-responsive" src="SMANSALGBTld.png" style="height:320px" alt="">
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form id="login-form" method="post" action="aksi_login.php" role="form">
                        <legend align="center">Login</legend>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                            <input type="text" name="email" placeholder="E-mail" autofocus required class="form-control" />
                        </div>
                        </br>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" name="password" placeholder="Password" required class="form-control" />
                        </div>
                        </br>
                        <div class="form-group">
                            <input type="submit" name="submit" value="Login" class="btn btn-primary btn-block" />
                        </div>
                        <div class="form-group">
                            <hr/>
                            <div class="center" class="col-sm-6" style="padding: 0;">Belum terdaftar? <a href="registrasi.php">Daftar di sini!</a></div>
                            <div class="col-sm-6 text-right brand" style="padding: 0;"></div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
			
			

        </div>
		</br>
		<div class="col-md-16">
		<div class="well" id="box" style="text-align: justify; font-size: 1em"> 
	    	Forum diskusi "DiskusiKan!" ini dibuat untuk siswa/i SMA ataupun SMK yang ada di sekitar Sitoluama. Sekolah yang tergabung dalam forum diskusi ini adalah SMKN 1 Laguboti, SMK Arjuna, SMA Unggul Del, dan SMAN 1 Laguboti. 
		</div> 
	</div>
        
        <hr>	
        <!-- /.container -->
    
    <!-- Page Content -->
    

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; TheroMedian 2017</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
