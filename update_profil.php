<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DiskusiKan! - Edit profil</title>

    
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/shop-homepage.css" rel="stylesheet">
    
</head>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
		
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
			
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
					<li>
						<a href="home2.php"><img src="logosdf.png" style="width:170px; height:20px"></a>
					</li>
                    <li>
                        <a href="school.php">Sekolah</a>
                    </li>
                    <li>
                        <a href="informasi.php">Informasi & Peristiwa</a>
                    </li>
                    <li>
                        <a href="profil.php">Profil</a>
                    </li>
                </ul>
				<ul class="nav navbar-nav navbar-right">
						<li><a href="home.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
				</ul>
            </div>
        </div>
    </nav>

		<?php 
			session_start();
			include "koneksi.php";
			$id=$_SESSION['nick'];;
			$ambil=$koneksi->prepare("SELECT * FROM user WHERE id_user='$id'");
			$ambil->execute();
	if(isset($_GET['update'])){

		$account_id=$id;	
				
		
		
		while($row=$ambil->fetch()){ 
		echo '

			<div class="container" id="container">
			
			
			<div class="col-md-6 col-md-offset-3">
		
				<div class="panel panel-primary">
					<div class="panel-heading" style="height:80px; text-align: center">
						<h2>Edit Profil</h2>
					</div>

					<div class="panel-body">

					<form method="post" action="update_profil.php?update_process='.$row['id_user'].'" enctype="multipart/form-data">
						Nama : 
						<input type="name" class="form-control" name="name" value="'.$row['username'].'" required><br>
						Password :
						<input type="password" class="form-control" name="password" value="'.$row['password'].'" required><br>
						Tanggal Lahir : 
						<input type="date" class="form-control" name="tgl_lahir" value="'.$row['tgl_lahir'].'" required><br>
						<div style="font-size : 1.2em">
						Jenis kelamin :
						<input type="radio" value="Perempuan" name="gender">Perempuan &emsp;&emsp; 
						<input type="radio" value="Laki-laki" name="gender">Laki-laki
						</div><br>
						Email : 
						<input type="email" class="form-control" name="email" value="'.$row['email'].'" required><br>
						No. Telepon : 
						<input type="no_hp" class="form-control" name="hp" value="'.$row['no_hp'].'" required><br>
						NIS : 
						<input type="NIS" class="form-control" name="nis" value="'.$row['nis'].'" required><br>
						
						<div style="text-align:right">
							<a href="profil.php"><button type="button" id="false">Cancel</button></a>
						
							<button type="submit" name="submit" id="true">Submit
							</button>
						</div>

						
						</form> 
					</div>
				</div>
				
			</div><br>

		</div>

			';

		
		}
	}

	else if(isset($_GET['update_process']))	{
		$account_id=$_GET['update_process'];

		$username = $_POST['name'];
		$password = $_POST['password'];
		$tgl_lahir= $_POST['tgl_lahir'];
		$gender = $_POST['gender'];
		if($gender==0){
			$gender='Perempuan';
		}
		else{
			$gender='Laki-laki';
		}
		$email= $_POST['email'];
		$hp = $_POST['hp'];
		$sekolah= $_POST['sekolah'];
		$nis= $_POST['nis'];
			
		
		
		$ambil2=$koneksi->prepare("UPDATE user SET username='$username',  password='$password', tgl_lahir='$tgl_lahir', gender='$gender', email='$email', no_hp='$hp', nis='$nis'  WHERE id_user='$account_id'");
		

		$update = $ambil2->execute();

		if ($update){
			echo '
				<script language="javascript">
					alert("Berhasil mengubah data profil.");
					document.location="profil.php";
				</script>
			';
		}
		else{
			echo '
				<script language="javascript">
					alert("Gagal melakukan perubahan.");
					document.location="profil.php";
				</script>
			';
		}
		
	}
		 
		
?>	

    <div class="container">

        <hr>

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
        </footer>

    </div>

    <script src="js/jquery.js"></script>

    <script src="js/bootstrap.min.js"></script>

</body>

</html>

