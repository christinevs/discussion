<?php
	session_start();
	include "koneksi.php";
	$id=$_SESSION['nick'];;
	$ambil=$koneksi->prepare("SELECT * FROM user WHERE id_user='$id'");
	$ambil->execute();
	//$result_set=$ambil->execute();
	
?>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DiskusiKan! - Profil</title>

    
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/shop-homepage.css" rel="stylesheet">
    
</head>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
		
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
			
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
					<li>
						<a href="home2.php"><img src="logosdf.png" style="width:170px; height:20px"></a>
					</li>
                    <li>
                        <a href="school.php">Sekolah</a>
                    </li>
                    <li>
                        <a href="informasi.php">Informasi & Peristiwa</a>
                    </li>
                    <li>
                        <a href="profil.php">Profil</a>
                    </li>
                </ul>
				<ul class="nav navbar-nav navbar-right">
						<li><a href="home.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
				</ul>
            </div>
        </div>
    </nav>

    <div class="container">

        <div class="col-md-12">
			<h2>Profil</h2>
		</div>
		<div class="col-md-3">
			<?php
				while($row=$ambil->fetch()){
					
				
			?>
			
		</div>
		<div class="col-md-10" id="profil">
		

			<table border="1.5" class="table table-hover table-bordered table-striped">
			
				<tr style="background-color: cornflowerblue; color:white">
					<td>Karateristik</td>
					<td>Data Diri</td>
				</tr>
				<tr style="background-color: white">
					<td>Nama</td>
					<td><?php echo $row['username']?></td>
				</tr>
				<tr>
					<td>Tanggal Lahir</td>
					<td><?php echo $row['tgl_lahir']?></td>
				</tr>
				<tr style="background-color: white">
					<td>Gender</td>
					<td><?php echo $row['gender']?></td>
				</tr>
				<tr style="background-color: white">
					<td>Email</td>
					<td><?php echo $row['email']?></td>
				</tr>
				<tr>
					<td>No. Handphone</td>
					<td><?php echo $row['no_hp']?></td>
				</tr>
				<tr>
					<td>NIS</td>
					<td><?php echo $row['nis']?></td>
				</tr>
				

				<?php  }?>

			</table>
		<?php 
			echo '<a href="update_profil.php?update='.$row['username'].'" >Edit Profil</a>';
		?>
		
	</div>

    </div>

    <div class="container">

        <hr>

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; TheroMedian 2017</p>
                </div>
            </div>
        </footer>

    </div>

    <script src="js/jquery.js"></script>

    <script src="js/bootstrap.min.js"></script>

</body>

</html>
