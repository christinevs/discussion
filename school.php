<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DiskusiKan! - Sekolah yang Tergabung</title>

    
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/shop-homepage.css" rel="stylesheet">
	<script type="text/javascript" src="fb/jquery.js"></script>
		<link rel="stylesheet" href="fb/jquery.fancybox.css?v=2.1.0" 
		type="text/css" media="screen" />
		<script type="text/javascript" src="fb/jquery.fancybox.pack.js?v=2.1.0"></script>
		<script type="text/javascript">$(document).ready(function() {
				$('.fancybox').fancybox();
			});
</script>
</head>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
		
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
			
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
					<li>
						<a href="home2.php"><img src="logosdf.png" style="width:170px; height:20px"></a>
					</li>
                    <li>
                        <a href="school.php">Sekolah</a>
                    </li>
                    <li>
                        <a href="informasi.php">Informasi & Peristiwa</a>
                    </li>
                    <li>
                        <a href="profil.php">Profil</a>
                    </li>
                </ul>
				<ul class="nav navbar-nav navbar-right">
						<li><a href="home.php"><span class="glyphicon glyphicon-log-in"></span> Keluar</a></li>
				</ul>
            </div>
        </div>
    </nav>

    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead" align="center">Forum Diskusi</p>
                <div class="list-group">
                    <a href="forumpendidikan/ForumPendidikan.html" class="list-group-item">Pendidikan</a>
                    <a href="forumpolitik/ForumPolitik.html" class="list-group-item">Politik</a>
                    <a href="forumhiburan/ForumHiburan.html" class="list-group-item">Hiburan</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">

					<div class="col-lg-12">
						<h1 class="page-header">Sekolah yang tergabung :</h1>
					</div>

            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                <a class="fancybox" href='SMKN1LGBT.jpg'
				data-fancybox-group="gallery" > <img class="img-polaroid" 
				src='SMKN1LGBT.jpg' width='190' height='143' alt="" /> </a>
				<p align="center">SMKN 1 Laguboti</p>
				<p align="center">Jln. Siborong-borong Kec. Laguboti</p>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                <a class="fancybox" href='SMKARJUNA.jpg'.jpg'
				data-fancybox-group="gallery" > <img class="img-polaroid" 
				src='SMKARJUNA.jpg' width='190' height='143' alt="" /> </a>
				<p align="center">SMK Arjuna</p>
				<p align="center">Jln. YP Arjuna Pintubosi Kec. Laguboti</p>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                <a class="fancybox" href='SUD.jpg'
				data-fancybox-group="gallery" > <img class="img-polaroid" 
				src='SUD.jpg' width='190' height='143' alt="" /> </a>
				<p align="center">SMA Unggul DEL</p>
				<p align="center">Jln. YP Arjuna, Sitoluama, Kec. Laguboti</p>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                <a class="fancybox" href='SMANSALGBTld.png'
				data-fancybox-group="gallery" > <img class="img-polaroid" 
				src='SMANSALGBTld.png' width='190' height='143' alt="" /> </a>
				<p align="center">SMAN 1 Laguboti</p>
				<p align="center">Jln. Sintong Marnipi, Kec. Laguboti</p>
            </div>
                </div>


            </div>

        </div>

    </div>

    <div class="container">
        <hr>
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; TheroMedian 2017</p>
                </div>
            </div>
        </footer>
    </div>



    <script src="js/bootstrap.min.js"></script>

</body>

</html>
