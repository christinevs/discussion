<!DOCTYPE html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="css/w3.css" rel="stylesheet">
    <link rel="stylesheet" href="css/hover.css" type="text/css" charset="utf-8" />
    <title>DiskusiKan! - Informasi&Peristiwa</title>

    
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/shop-homepage.css" rel="stylesheet">
     <style>
#myDIV1,#myDIV2,#myDIV3 {
    display:none;
}
</style>
</head>

<body>
    
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
		
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
			
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
					<li>
						<a href="home2.php"><img src="logosdf.png" style="width:170px; height:20px"></a>
					</li>
                    <li>
                        <a href="school.php">Sekolah</a>
                    </li>
                    <li>
                        <a href="informasi.php">Informasi & Peristiwa</a>
                    </li>
                    <li>
                        <a href="profil.php">Profil</a>
                    </li>
                </ul>
				<ul class="nav navbar-nav navbar-right">
						<li><a href="home.php"><span class="glyphicon glyphicon-log-in"></span> Keluar</a></li>
				</ul>
            </div>
        </div>
    </nav>
	
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead" align="center">Forum Diskusi</p>
                <div class="list-group">
                    <a href="forumpendidikan/ForumPendidikan.html" class="list-group-item">Pendidikan</a>
                    <a href="forumpolitik/ForumPolitik.html" class="list-group-item">Politik</a>
                    <a href="forumhiburan/ForumHiburan.html" class="list-group-item">Hiburan</a>
                </div>
            </div>
		<div class="col-md-9">

                <div class="row carousel-holder">
				<div class="col-lg-12">
						<h1 class="page-header">Informasi & Peristiwa</h1>
					</div>
            <div class="w3-content w3-teal">
      <div class="w3-container">
        <div class="w3-container w3-margin">
        <div class="w3-third w3-container ">
          <br><img src="image/beasiswa.jpg" style="width:80%; height:100%;min-height:200px">
        </div>
          <div id="modal01" class="w3-modal" onclick="this.style.display='none'">
            <span class="w3-closebtn w3-hover-red w3-container w3-padding-hor-16 w3-display-topright">&times;</span>
            <div class="w3-modal-content w3-animate-zoom">
            <img src="image/beasiswa2.jpg" style="width:60%">
            </div>
          </div>
        <div class="w3-twothird">
          <h3><i>Beasiswa</i></h3>
          <p class="w3-justify">
            Dalam memajukan kualitas pendidikan bagi kalangan para pelajar, pemerintah sudah mengalokasikan banyak jenis beasiswa bagi para pelajar melalui berbagai lembaga seperti Kementrian Pendidikan dan Kebudayaan, kerja sama dengan pihak swasta dan lain-lain. Untuk pelajar sekolah ada beasiswa siswa miskin bagi siswa/i yang kurang mampu dalam hal ekonomi, beasiswa bidikmisi, beasiswa prestasi, beasiswa dari kalangan pejabat seperti presiden, dan masih banyak lagi.
            <br>
            Berikut ini adalah beberapa jenis beasiswa yang diberikan bagi kalangan pelajar secara khusus bagi pelajar SMA/SMK sederajat.
            <div id="myDIV1">
            <h4>1.Beasiswa Tanoto</h4>
            <img src="image/tanoto.jpg" style="width: 70%">
            <p>
             Beasiswa Tanoto merupakan sebuah perwujudan kepedulian terhadap sesama untuk menanggulangi kemiskinan melalui pendidikan, pemberdayaan dan kualitas hidup. Berikut ini contoh beasiswa yang diberikan oleh pihak Tanoto bagi kalangan pelajar SMA/SMK sederajat.
             <li>Regional Champion Scholarship</li>
             <p>
               Beasiswa ini diperuntukkan bagi siswa kelas XII SMA/SMK yang ada di Sumatera Utara, Riau dan Jambi yang memiliki keinginan untuk melanjutkan pendidikan ke Perguruan Tinggi Negri (PTN) atau Swasta (PTS).
             </p>

             <li>Beasiswa Sekolah Menengah</li>
             <p>
               Beasiswa sekolah menengah adalah beasiswa yang diberikan kepada siswa SMP dan SMA yang berasal dari keluarga kurang mampu yang ada di Medan dan Belawa, Sumatera Utara.
             </p>

             <li>Beasiswa Sayap Garuda</li>
             <p>
               Beasiswa Sayap Garuda adalah beasiswa yang diperuntukkan bagi siswa/i berprestasi di daerah pedesaan dan berlaku untuk semua lokasi sekolah di Jambi, Riau, dan Sumatera.
             </p>
            </p>
          </div>
          </p>  
          <a href="#"><input type="button" class="w3-btn w3-red w3-ripple" style="float:right" onclick="myFunction1()" value="Lanjut Baca" id="myButton1"></input></a>
        </div>
      </div>

      <div class="w3-container w3-margin">
        <div class="w3-third w3-container ">
          <br><img src="image/university.jpg" style="width:80%; height:100%;min-height:200px" onclick="document.getElementById('modal02').style.display='block'">
        </div>
          <div id="modal02" class="w3-modal" onclick="this.style.display='none'">
            <span class="w3-closebtn w3-hover-red w3-container w3-padding-hor-16 w3-display-topright">&times;</span>
            <div class="w3-modal-content w3-animate-zoom">
            <img src="image/wisuda.jpg" style="width:60%">
            </div>
          </div>
        <div class="w3-twothird">
          <h3><i>Perguruan Tinggi</i></h3>
          <p class="w3-justify">
            Pendidikan tinggi pada hakekatnya merupakan upaya sadar untuk meningkatkan kadar ilmu pengetahuan dan pengamalan bagi mahasiwa dan lembaga dimana upaya itu bergulir menuju sasaran-sasaran pada tujuan yang ditetapkan.
            <br>
            Di Indonesia, perguruan tinggi dapat berbentuk akademi, institut, politeknik, sekolah tinggi, dan universitas.
            Berikut ini adalah beberapa perguruan tinggi terfavorit di Indonesia.
            <div id="myDIV2">
            <h4>1.Institut Teknologi Bandung (ITB)</h4>
            <img src="image/itb.jpg" style="width: 60%">
            <p>
              ITB berlokasi di kota Bandung yang diresmikan pada tanggal 2 Maret 1959. ITB memiliki 20 program studi yang terakreditasi secara internasional. Kampus utama ITB saat ini merupakkan lokasi dari sekolah tinggi teknik pertama di Indonesia sekaligus lembaga pendidikan pertama di Hindia Belanda.
            </p>
            <h4>2.Universitas Gadjah Mada (UGM)</h4>
            <img src="image/ugm.jpg" style="width: 60%">
            <p>
              UGM merupakan univeristas negri yang didirikan oleh Pemerintah Republik Indonesia pada tanggal 19 Desember 1949. UGM berlokasi di kampus Bulaksumur Yogyakarta.
            </p>
            </p>
          </div>
          </p>
          <a href="#"><input type="button" class="w3-btn w3-red w3-ripple" style="float:right" onclick="myFunction2()" value="Lanjut Baca" id="myButton2"></input></a>
        </div>
      </div>
      <div class="w3-container w3-margin">
        <div class="w3-third w3-container ">
          <br><img src="image/prestasi.jpg" style="width:80%; height:100%;min-height:200px" onclick="document.getElementById('modal02').style.display='block'">
        </div>
          <div id="modal03" class="w3-modal" onclick="this.style.display='none'">
            <span class="w3-closebtn w3-hover-red w3-container w3-padding-hor-16 w3-display-topright">&times;</span>
            <div class="w3-modal-content w3-animate-zoom">
            <img src="image/prestasi2.png" style="width:60%">
            </div>
          </div>
        <div class="w3-twothird">
          <h3><i>Prestasi</i></h3>
          <p class="w3-justify">
            Indonesia patut berbangga karena beberapa waktu yang lalu Indonesia telah meraih banyak prestasi di tingkat internasional melalui putra-putri bangsa yang di utus oleh negara untuk mengikuti berbagai jenis kompetisi. Ada yang dibidang sains, ketangkasan, seni dan masih banyak lagi.
            Berikut beberapa prestasi yang telah diraih anak-anak bangsa.
            <div id="myDIV3">
            <h4>1.Empat medali dari olimpiade internasional Biologi</h4>
            <img src="image/biologi.jpg" style="width: 60%">
            <p>
              Tim olimpiade Biologi Indonesia yang terdiri dari empat siswa SMA menyabet satu medali emas, dua medali perak dan satu medali perunggu dalam ajang kompetisi International Biology Olympiad (IBO) ke 26 yang berlangsung di Aarhus, Denmark. Empat siswa itu adalah Maria Patricia Inggriani, Valdi Ven Japranata, Hana Fauzyahyah Hanifin, dan Nagita Gianty Annisa.
            </p>
            <h4>2.Medali emas olimpiade astronomi dan astrofisika internasional</h4>
            <img src="image/astronomi.jpg" style="width: 70%">
            <p>
              Joandy Leonata Pratama, siswa SMA Sutomo 1, Medan, Sumatera Utara berhasil menjadi pemenang dalam Olimpiade Internasional Astronomi dan Astrofisika atau International Olympiad on Astronomy and Astrophysics (IOAA) 2015. Dari seluruh peraih medali emas, ia berada di peringkat pertama dengan nilai tertinggi. 
            </p>
            <h4>3.Tim paduan suara anak Indonesia raih prestasi tingkat internasional</h4>
            <img src="image/rcc.jpg" style="width: 70%">
            <p>
              Kelompok paduan suara 'the Resonanz Children Choir' yang diikuti 48 anak berusia sembilan tahun ke atas ini berkompetisi di empat kategori untuk ajang The Golden Gate International Choral Festival 2015. Kategori tersebut adalah lagu daerah, historis, gospel, dan kontemporer. Kerennya, mereka berhasil meraih dua juara pertama untuk kategori lagu daerah dan historis, juara dua di kategori gospel, dan juara tiga untuk kategori kontemporer. 
            </p>
            </p>
          </div>
          </p>  
          <a href="#"><input type="button" class="w3-btn w3-red w3-ripple" style="float:right" onclick="myFunction3()" value="Lanjut Baca" id="myButton3"></input></a>
        </div>
      
      
      </div>
    </div>
<script>
function myFunction1() {
    var elem = document.getElementById("myButton1");
    var x = document.getElementById('myDIV1');
    if (x.style.display === 'block') {
        x.style.display = 'none';
        elem.value = "Lanjut Baca";
    } else {
        x.style.display = 'block';
        elem.value = "Tutup";
    }
}

function myFunction2() {
    var elem = document.getElementById("myButton2");
    var x = document.getElementById('myDIV2');
    if (x.style.display === 'block') {
        x.style.display = 'none';
        elem.value = "Lanjut Baca";
    } else {
        x.style.display = 'block';
        elem.value = "Tutup";
    }
}

function myFunction3() {
    var elem = document.getElementById("myButton3");
    var x = document.getElementById('myDIV3');
    if (x.style.display === 'block') {
        x.style.display = 'none';
        elem.value = "Lanjut Baca";
    } else {
        x.style.display = 'block';
        elem.value = "Tutup";
    }
}
</script>
</div>
</div>
</div>

    <div class="container">

        <hr>
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; TheroMedian 2017</p>
                </div>
            </div>
        </footer>

    </div>

    <script src="js/jquery.js"></script>

    <script src="js/bootstrap.min.js"></script>

</body>

</html>
